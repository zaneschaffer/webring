let sitelist = document.getElementById("sitelist")

fetch('./sites.json')
    .then(response => response.json())
    .then(data => {
        data.forEach(site => {
            let a = document.createElement('a');
            let aText = document.createTextNode(site.name)
            a.appendChild(aText)
            a.href=site.url
            sitelist.appendChild(a)
        });
    })
